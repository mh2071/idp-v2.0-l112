#include <Arduino.h>
#include <Adafruit_MotorShield.h>
#include "motion_funcs.h"
#include "sensor_readers.h"
#include "Ticker.h"

// full test run. caution advised, as this was unfortunately not run even once due to significant delays in the mech dept.

#define min_us_distance 15
#define min_ir_distance 8

// initialise motors
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *leftRear = AFMS.getMotor(1);
Adafruit_DCMotor *rightRear = AFMS.getMotor(2);


// important variables under main
int frontArrayLineCounter = 0;
int backArrayLineCounter = 0;
int fls = 0;
int bls = 0;
int speed = 0;
float us = 0;
int starttime = 0;
int secondstage = 0;
int endtime = 0;
int type;
int under = 0;

// pre-initialisation to avoid cluttering
void searchAndDropStage(int time, int speed, Adafruit_DCMotor *lr, Adafruit_DCMotor *rr);
void searchForThird(int time1, int time2, int speed, Adafruit_DCMotor *lr, Adafruit_DCMotor *rr);


void setup() {
  // sets pin modes, awaiting start
  setPinModes();
  aperture.attach(aperturePin);
  height.attach(heightPin);
  servoOpen(height_min, height_max, height);
  delay(500);
  servoClose(aperture_min, aperture_max, aperture);
  Serial.begin(9600);
  if (!AFMS.begin()) {         // create with the default frequency 1.6KHz
  // if (!AFMS.begin(1000)) {  // OR with a different frequency, say 1KHz
    Serial.println("Could not find Motor Shield. Check wiring.");
    while (1);
  }
  Serial.println("Motor Shield found.");
  attachInterrupt(digitalPinToInterrupt(buttonPin), toggleStart, CHANGE);
  while (!start){
    delay(10);
  }
  Serial.println("Beginning test run");
  digitalWrite(inOperationLed, HIGH);
}

void loop() {
  us = infraredRead();
  fls = frontLineSense();
  bls = rearLineSense();
  // first search and drop stage
  searchAndDropStage(25000, 8000, 225, leftRear, rightRear);
  junctionDetect = false;
  // second search and drop stage
  searchAndDropStage(10000, 8000, 225, leftRear, rightRear);
  junctionDetect = false;  
  // search for third
  searchForThird(12000, 200, 225, leftRear, rightRear);  
  // two junctions expected after third pickup
  junctionDetect = false;
  while (!junctionDetect){
    highLineFollow(bls, speed, leftRear, rightRear, backArrayLineCounter);
  }
  junctionDetect = false;
  while (!junctionDetect){
    highLineFollow(bls, speed, leftRear, rightRear, backArrayLineCounter);
  } 
  stop(leftRear, rightRear);
  while(1);
}

void searchAndDropStage(int time, int time_g, int speed, Adafruit_DCMotor *lr, Adafruit_DCMotor *rr) {
  // starts timer, follows line without detecting cube until time limit is reached
  starttime = millis();
  endtime = millis();
  while ((endtime - starttime) < time) {
    fls = frontLineSense();
    deafLineFollow(fls, speed, lr, rr, frontArrayLineCounter);
    endtime = millis();
  }
  // resets junctionDetect, seeks cube
  junctionDetect = false;
  if (infraredRead() > min_ir_distance and !junctionDetect){
    fls = frontLineSense();
    deafLineFollow(fls, 140, lr, rr, frontArrayLineCounter);
  } else {
    // detects, picks up and identifies cube
    junctionDetect = false;
    stop(leftRear, rightRear);
    clawCollect(aperture, height);
    type = cubeType();
    starttime = millis();
    endtime = millis();
    // reverses over ramp for 8s
    while ((endtime - starttime) < time_g) {
      highLineFollow(bls, speed, lr, rr, backArrayLineCounter);
      endtime = millis();
    }
    junctionDetect = false;
    // follows line until dropoff junction
    while (!junctionDetect) {
      bls = rearLineSense();
      highLineFollow(bls, speed, lr, rr, backArrayLineCounter);
    }
    // cube dropoff
    stop(leftRear, rightRear);
    rotate90(cubeType, rotationGain, lr, rr, speed);
    clawDropoff(aperture, height);
    delay(100);
    rotate90(!cubeType, rotationGain, lr, rr, speed);
  }
}

void searchForThird(int time1, int time2, int speed, Adafruit_DCMotor *lr, Adafruit_DCMotor *rr) {
  starttime = millis();
  endtime = millis();
  // goes forward until closer boundary of third cube square
  while ((endtime - starttime) < time1) {
    deafLineFollow(fls, speed, lr, rr, frontArrayLineCounter);
    endtime = millis();
  }
  junctionDetect = false;
  if (infraredRead() > min_ir_distance and !junctionDetect){
    deafLineFollow(fls, speed, lr, rr, frontArrayLineCounter);
  }
  for (int i = 0; i < 50; i++){
    move(0, 100, leftRear, rightRear);
    delay(10);
  }
  starttime = millis();
  endtime = millis();

  // sweeps a certain acute angle for the cube. adjust parameters according to desired range
  while ((endtime - starttime) < 4 * time2){
    if(infraredRead() > 15){
      if ((endtime - starttime) < time2){
        blockSweep(1, leftRear, rightRear, speed);
      } else if ((endtime - starttime) < 3 * time2) {
        blockSweep(0, leftRear, rightRear, speed);
      } else {
        blockSweep(1, leftRear, rightRear, speed);
      }
    } else {
      if (infraredRead > min_ir_distance) {
        move(1, 100, leftRear, rightRear);
      } else {
        clawCollect(aperture, height);
        type = cubeType();
        break;
      }        
      }
      endtime = millis();
  }
  // ignores junctions / infrared to return to starting half
  while ((endtime - starttime) < 8000) {
    highLineFollow(bls, speed, lr, rr, backArrayLineCounter);
    endtime = millis();
  }
  junctionDetect = false;
  // reaches junction
  while (!junctionDetect) {
    bls = rearLineSense();
    highLineFollow(bls, speed, lr, rr, backArrayLineCounter);
  }
  // drops cube
  stop(leftRear, rightRear);
  rotate90(cubeType, rotationGain, lr, rr, speed);
  clawDropoff(aperture, height);
  delay(100);
  rotate90(!cubeType, rotationGain, lr, rr, speed);
}