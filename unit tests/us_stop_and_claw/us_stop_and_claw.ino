#include <Arduino.h>
#include <Adafruit_MotorShield.h>
#include "motion_funcs.h"
#include "led_signals.h"
#include "sensor_readers.h"
#include "Ticker.h"

// cascading infrared stoppage with claw operation

#define min_ir_distance 8

// initialise motors
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *leftRear = AFMS.getMotor(1);
Adafruit_DCMotor *rightRear = AFMS.getMotor(2);

// important variables within main script
int frontArrayLineCounter = 0;
int backArrayLineCounter = 0;
int fls = 0;
int bls = 0;
int speed = 0;
float ir = 0;
int starttime = 0;
int secondstage = 0;
int endtime = 0;
int type;
int under = 0;

void setup() {
  // set pin modes and move servos to presets, awaiting button press for start
  setPinModes();
  aperture.attach(aperturePin);
  height.attach(heightPin);
  servoOpen(height_min, height_max, height);
  servoClose(aperture_min, aperture_max, aperture);
  Serial.begin(9600);
  if (!AFMS.begin()) {         // create with the default frequency 1.6KHz
  // if (!AFMS.begin(1000)) {  // OR with a different frequency, say 1KHz
    Serial.println("Could not find Motor Shield. Check wiring.");
    while (1);
  }
  Serial.println("Motor Shield found.");
  attachInterrupt(digitalPinToInterrupt(buttonPin), toggleStart, CHANGE);
  while (!start){
    delay(10);
  }
  Serial.println("Beginning test run");
  digitalWrite(inOperationLed, HIGH);
}

void loop() {
  // follows line until cube is detected. claws it and puts it right back where it is.
  ir = infraredRead();
  Serial.println(ir);
  fls = frontLineSense();
  bls = rearLineSense();
  if (ir > min_ir_distance){
    deafLineFollow(fls, 180, leftRear, rightRear, frontArrayLineCounter);
  }
  else {
    stop(leftRear, rightRear);
    clawCollect(aperture, height);
    Serial.println("collected");
    delay(1000);
    clawDropoff(aperture, height);
    Serial.println( "dropped");
  }
}
