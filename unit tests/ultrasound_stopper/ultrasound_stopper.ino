#include <Arduino.h>
#include <Adafruit_MotorShield.h>
#include "motion_funcs.h"
#include "led_signals.h"
#include "sensor_readers.h"
#include "Ticker.h"

// second unit test. confirm that distance detection results in robot stopping at desired distance to cube.
// started off as ultrasound, ended up as infrared

#define min_us_distance 15
#define min_ir_distance 8

Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *leftRear = AFMS.getMotor(1);
Adafruit_DCMotor *rightRear = AFMS.getMotor(2);

int frontArrayLineCounter = 0;
int backArrayLineCounter = 0;
int fls = 0;
int bls = 0;
int speed = 0;
float us = 0;
int starttime = 0;
int secondstage = 0;
int endtime = 0;
int type;
int under = 0;

void setup() {
  // startup sequence with pins
  setPinModes();
  aperture.attach(aperturePin);
  Serial.begin(9600);
  if (!AFMS.begin()) {         // create with the default frequency 1.6KHz
  // if (!AFMS.begin(1000)) {  // OR with a different frequency, say 1KHz
    Serial.println("Could not find Motor Shield. Check wiring.");
    while (1);
  }
  Serial.println("Motor Shield found.");
  attachInterrupt(digitalPinToInterrupt(buttonPin), toggleStart, CHANGE);
  // awaits button press to move onto void loop
  while (!start){
    delay(10);
  }
  Serial.println("Beginning test run");
  digitalWrite(inOperationLed, HIGH);
}

void loop() {
  // single case of line following and stoppage upon cube detection. 
  // requires hard reset every time but strong and robust
  Serial.println(infraredRead());
  fls = frontLineSense();
  if (infraredRead() > min_us_distance){
    deafLineFollow(fls, 180, leftRear, rightRear, frontArrayLineCounter);
  }
  else {
    stop(leftRear, rightRear);
    while(1);
  }
}
