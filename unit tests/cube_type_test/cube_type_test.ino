#include <Arduino.h>
#include <Adafruit_MotorShield.h>
#include "motion_funcs.h"
#include "led_signals.h"
#include "sensor_readers.h"
#include "Ticker.h"

// collects two cubes and returns to starting box.

#define min_us_distance 15
#define min_ir_distance 8

Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *leftRear = AFMS.getMotor(1);
Adafruit_DCMotor *rightRear = AFMS.getMotor(2);


int frontArrayLineCounter = 0;
int backArrayLineCounter = 0;
int fls = 0;
int bls = 0;
int speed = 0;
float us = 0;
int starttime = 0;
int secondstage = 0;
int endtime = 0;
int type;
int under = 0;

void setup() {
  // set pin modes, preset servo positions
  setPinModes();
  aperture.attach(aperturePin);
  height.attach(heightPin);
  servoOpen(0,70,aperture);
  Serial.begin(9600);
  if (!AFMS.begin()) {         // create with the default frequency 1.6KHz
  // if (!AFMS.begin(1000)) {  // OR with a different frequency, say 1KHz
    Serial.println("Could not find Motor Shield. Check wiring.");
    while (1);
  }
  Serial.println("Motor Shield found.");
  attachInterrupt(digitalPinToInterrupt(buttonPin), toggleStart, CHANGE);
  while (!start){
    delay(10);
  }
  Serial.println("Beginning test run");
  digitalWrite(inOperationLed, HIGH);
}

void loop() {
  // reading and printing cubetype with ema time 0.2. tune threshold found in sensor_readers to ensure accuracy
  Serial.print("Analog value:");
  Serial.println(analogRead(IRPin));
  Serial.print("Cube type:");
  Serial.println(cubeType(0.2));
  delay(100);
}
