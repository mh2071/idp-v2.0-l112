// stop at horizontal line
// Go forward for 3-4s
// Stop
// Sweep time delay to the right, record US array
// Look at much smaller distance (smaller than a value)
// Stop as soon as observed unless time limit is satisfied, go x forward, close claw, go x backward, line correct
// Else, do the same with the other side
// Return to line, backtrack, drop off cube.

#include <Arduino.h>
#include <Adafruit_MotorShield.h>
#include "motion_funcs.h"
#include "led_signals.h"
#include "sensor_readers.h"
#include "Ticker.h"

// sweeps third cube. see above for intended sequence of actions.

#define min_ir_distance = 8


// initialise motors
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *leftRear = AFMS.getMotor(1);
Adafruit_DCMotor *rightRear = AFMS.getMotor(2);


// important variables in main
int frontArrayLineCounter = 0;
int backArrayLineCounter = 0;
int fls = 0;
int bls = 0;
int speed = 0;
float us = 0;
int starttime = 0;
int secondstage = 0;
int endtime = 0;
int type;
int under = 0;

void sweeper(bool direction, int speed);

void setup() {
  // set pin modes, adjust servos, await button press
  setPinModes();
  aperture.attach(aperturePin);
  height.attach(directionPin);
  aperture.open(0,70,aperture);
  Serial.begin(9600);
  if (!AFMS.begin()) {         // create with the default frequency 1.6KHz
  // if (!AFMS.begin(1000)) {  // OR with a different frequency, say 1KHz
    Serial.println("Could not find Motor Shield. Check wiring.");
    while (1);
  }
  Serial.println("Motor Shield found.");
  attachInterrupt(digitalPinToInterrupt(buttonPin), toggleStart, CHANGE);
  while (!start){
    delay(10);
  }
  Serial.println("Beginning test run");
  // starting amber led
  digitalWrite(inOperationLed, HIGH);
}

void loop() {
  us = ultrasoundRead();
  fls = frontLineSense();
  rls = rearLineSense();

  // requires placement on ramp while headed towards cube.

  if (!junctionDetect){
    deafLineFollow(fls, speed, leftRear, rightRear, frontArrayLineCounter);
  } else {
    stop(leftRear, rightRear);
    sweeper(1, 160);
    delay(100);
    sweeper(0, 160);
    }
}

void sweeper(bool direction, int speed){
  for (int i = 0; i <= 100; i++){
        // incrementally sweeps for block, goes forward to collect it
        blockSweep(direction, leftRear, rightRear, speed);
        if (infraredRead() < 35) {
          stop(leftRear, rightRear);
          starttime = millis();
          while(ultrasoundSweep(0.5) > 15){
            move(1, speed, leftRear, rightRear);
          }
          stop(leftRear,rightRear);
          endtime = millis();
          clawCollect(aperture, height);
          for (int i = 0; i < (int)((endtime - starttime) / 20); i++) {
            move(0, 160, leftRear, rightRear);
          }
        }
      }
  for (int i = 0; i <= 100; i++){
    // incrementally sweeps for block, goes forward to collect it
      blockSweep(!direction, leftRear, rightRear, speed);
        if (infraredRead() < 35) {
          stop(leftRear, rightRear);
          starttime = millis();
          while(ultrasoundSweep(0.5) > 15){
            move(1, speed, leftRear, rightRear);
          }
          stop(leftRear,rightRear);
          endtime = millis();
          clawCollect(aperture, height);
          for (int i = 0; i < (int)((endtime - starttime) / 20); i++) {
            move(0, 160, leftRear, rightRear);
          }
        }
  }
}
