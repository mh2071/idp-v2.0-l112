#include <Arduino.h>
#include <Adafruit_MotorShield.h>
#include "motion_funcs.h"
#include "led_signals.h"
#include "sensor_readers.h"
#include "Ticker.h"

// test for horizontal line detection, and counting, aka checkpoints.
// increase time delay for front array reactivation if not working.
// name suggests "Front Array Line Counting"
// the code later evolved to only identify junctions and not count them

// motor initialisation
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *leftRear = AFMS.getMotor(1);
Adafruit_DCMotor *rightRear = AFMS.getMotor(2);

//important variables
int frontArrayLineCounter = 0;
int backArrayLineCounter = 0;
int fls = 0;
int bls = 0;
int speed = 0;
float us = 0;
int starttime = 0;
int secondstage = 0;
int endtime = 0;
int type;
int under = 0;

void setup() {
  // startup sequence sets pins and waits for button press
  setPinModes();

  Serial.begin(9600);
  if (!AFMS.begin()) {         // create with the default frequency 1.6KHz
  // if (!AFMS.begin(1000)) {  // OR with a different frequency, say 1KHz
    Serial.println("Could not find Motor Shield. Check wiring.");
    while (1);
  }
  Serial.println("Motor Shield found.");
  attachInterrupt(digitalPinToInterrupt(buttonPin), toggleStart, CHANGE);
  while (!start){
    delay(10);
  }
  Serial.println("Beginning test run");
  digitalWrite(inOperationLed, HIGH);
}

void loop() {
  // going forward and backward between junctions
  // front array detects forwards and rear array backwards
  // junctionDetect overwritten at each stage
  while (!junctionDetect) {
    fls = frontLineSense();
    deafLineFollow(fls, 140, leftRear, rightRear, frontArrayLineCounter);
  }
  Serial.println(frontArrayLineCounter);
  stop(leftRear, rightRear);
  delay(1000);
  junctionDetect = false;
  backArrayLineCounter = 0;
  while (!junctionDetect) {
    bls = rearLineSense();
    highLineFollow(bls, 140, leftRear, rightRear, backArrayLineCounter);
  }
  Serial.println(backArrayLineCounter);
  stop(leftRear, rightRear);
  delay(1000);
  junctionDetect = false;
}