#include <Arduino.h>
#include <Adafruit_MotorShield.h>
#include "motion_funcs.h"
#include "led_signals.h"
#include "sensor_readers.h"
#include "Ticker.h"

// unit test for rotating towards dropoff zones upon detecting junction.

#define min_us_distance 8

// motors initialised
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *leftRear = AFMS.getMotor(1);
Adafruit_DCMotor *rightRear = AFMS.getMotor(2);

//important variables within main file
int frontArrayLineCounter = 0;
int backArrayLineCounter = 0;
int fls = 0;
int bls = 0;
int speed = 0;
float us = 0;
int starttime = 0;
int secondstage = 0;
int endtime = 0;
int type;
int under = 0;

void setup() {
  // set pin modes, awaiting button press for operation
  setPinModes();
  Serial.begin(9600);
  if (!AFMS.begin()) {         // create with the default frequency 1.6KHz
  // if (!AFMS.begin(1000)) {  // OR with a different frequency, say 1KHz
    Serial.println("Could not find Motor Shield. Check wiring.");
    while (1);
  }
  Serial.println("Motor Shield found.");
  attachInterrupt(digitalPinToInterrupt(buttonPin), toggleStart, CHANGE);
  while (!start){
    delay(10);
  }
  Serial.println("Beginning test run");
  digitalWrite(inOperationLed, HIGH);
}

void loop() {
  // follows line forwards until junction is detected and sweeps +/-90deg upon detection analogous to dropoff
  fls = frontLineSense();
  if (!junctionDetect){
    deafLineFollow(fls, 150, leftRear, rightRear, frontArrayLineCounter);
  } else {
    stop(leftRear, rightRear);
    rotate90(0, 3000, leftRear, rightRear, 160);
    delay(100);
    rotate90(1, 3000, leftRear, rightRear, 160);
    stop(leftRear, rightRear);
    junctionDetect = false;
  }
}
