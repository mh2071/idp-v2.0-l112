#include <Arduino.h>
#include <Adafruit_MotorShield.h>
#include "motion_funcs.h"
#include "led_signals.h"
#include "sensor_readers.h"
#include "Ticker.h"

// initial test for servo. use code to clamp interval parameters found in config.
Adafruit_MotorShield AFMS = Adafruit_MotorShield();

void setup() {
  setPinModes();
  Serial.begin(9600);
  if (!AFMS.begin()) {         // create with the default frequency 1.6KHz
  // if (!AFMS.begin(1000)) {  // OR with a different frequency, say 1KHz
    Serial.println("Could not find Motor Shield. Check wiring.");
    while (1);
  }
  Serial.println("Motor Shield found.");
  attachInterrupt(digitalPinToInterrupt(buttonPin), toggleStart, CHANGE);
  while (!start){
    delay(10);
  }
  Serial.println("Beginning test run");
  aperture.attach(aperturePin);
  height.attach(heightPin);
  digitalWrite(inOperationLed, HIGH);
}

void loop() {
  // sweep parameters to observe range of motion. set aperture and height settings in motion_funcs.h appropriately
  servoClose(70, 120, height);
  Serial.println("height");
  delay(1000);
  servoClose(0, 120, aperture);
  Serial.println("aperture");
  delay(1000);
  servoOpen(70, 120, height);
  delay(1000);
  servoOpen(0, 120, aperture);
  Serial.println("aperture");
  delay(1000);
}
