#include <Arduino.h>
#include <Adafruit_MotorShield.h>
#include "motion_funcs.h"
#include "led_signals.h"
#include "sensor_readers.h"
#include "Ticker.h"

// collects and drops two cubes and returns to starting box.

// constants for distance sensors
#define min_us_distance 15
#define min_ir_distance 8

// initialising motors, and relevant constants and variables
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *leftRear = AFMS.getMotor(1);
Adafruit_DCMotor *rightRear = AFMS.getMotor(2);
int frontArrayLineCounter = 0;
int backArrayLineCounter = 0;
int fls = 0;
int bls = 0;
int speed = 225;
float us = 0;
int starttime = 0;
int secondstage = 0;
int endtime = 0;
int type;
int under = 0;

//pre-loop function definition to avoid cluttering, see end of file for full func.
void searchAndDropStage(int time, int speed, Adafruit_DCMotor *lr, Adafruit_DCMotor *rr);

void setup() {
  // setting pin modes, attaching pwm enabled pins to servo
  // returning servos to prescribed positions
  // awaiting button press to move onto main loop
  setPinModes();
  aperture.attach(aperturePin);
  height.attach(heightPin);
  servoOpen(height_min, height_max, height);
  delay(500);
  servoClose(aperture_min, aperture_max, aperture);
  Serial.begin(9600);
  if (!AFMS.begin()) {         // create with the default frequency 1.6KHz
  // if (!AFMS.begin(1000)) {  // OR with a different frequency, say 1KHz
    Serial.println("Could not find Motor Shield. Check wiring.");
    while (1);
  }
  Serial.println("Motor Shield found.");
  attachInterrupt(digitalPinToInterrupt(buttonPin), toggleStart, CHANGE);
  while (!start){
    delay(10);
  }
  Serial.println("Beginning test run");
  // amber led pin toggled, flashing handled by physical circuitry
  digitalWrite(inOperationLed, HIGH);
}

void loop() {
  us = ultrasoundRead();
  fls = frontLineSense();
  // search and drop for first cube. to traverse the ramp from the starting square
  searchAndDropStage(15000, speed, leftRear, rightRear);
  // search and drop for second cube. delay encompasses traversing the ramp from the dropoff junction
  searchAndDropStage(10000, speed, leftRear, rightRear);
  // final reversing sequence
  // setting junctiondetect twice to prevent false triggers
  // only stop when rear line of starting area is detected
  junctionDetect = false;  
  while (!junctionDetect){
    highLineFollow(bls, speed, leftRear, rightRear, backArrayLineCounter);
  }
  junctionDetect = false;
  while (!junctionDetect){
    highLineFollow(bls, speed, leftRear, rightRear, backArrayLineCounter);
  } 
  stop(leftRear, rightRear);
  while(1);
}

// function for full search and drop sequence. see motionfuncs.h for full descriptions
void searchAndDropStage(int time, int speed, Adafruit_DCMotor *lr, Adafruit_DCMotor *rr) {
  // timing the initial leaving stage to prevent false distance triggers at ramp ascent-descent
  starttime = millis();
  endtime = millis();
  while ((endtime - starttime) < time) {
    fls = frontLineSense();
    deafLineFollow(fls, speed, lr, rr, frontArrayLineCounter);
    endtime = millis();
  }
  // junctiondetect set to false since deaflinefollow automatically runs this check
  // cube search initiated, slow down to ensure more precise approach
  junctionDetect = false;
  if (infraredRead() > min_ir_distance){
    fls = frontLineSense();
    deafLineFollow(fls, 140, lr, rr, frontArrayLineCounter);
  } else {
    // cube detected and junctions up until that point ignored
    junctionDetect = false;
    stop(leftRear, rightRear);
    // cube collected and its type identified
    clawCollect(aperture, height);
    type = cubeType(0.1);
    starttime = millis();
    endtime = millis();
    while ((endtime - starttime) < 8000) {
      // reversing out of farther half without triggering dropoff sequence
      // delay must be applicable for both first and second cycle
      bls = rearLineSense();
      highLineFollow(bls, speed, lr, rr, backArrayLineCounter);
      endtime = millis();
    }
    // reversing to put cube into desired location, junctionDetect false before just in case
    junctionDetect = false;
    while (!junctionDetect) {
      bls = rearLineSense();
      highLineFollow(bls, speed, lr, rr, backArrayLineCounter);
    }
    // dropoff according to cubeType, generalised according to cubetype (returns 0 or 1, compatible with direction arguments)
    stop(leftRear, rightRear);
    rotate90(type, rotationGain, lr, rr, speed);
    clawDropoff(aperture, height);
    delay(100);
    rotate90(!type, rotationGain, lr, rr, speed);
  }
}

