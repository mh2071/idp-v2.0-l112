#include <Arduino.h>
#include <Adafruit_MotorShield.h>
#include "motion_funcs.h"
#include "led_signals.h"
#include "sensor_readers.h"
#include "Ticker.h"

// test whether forward and backward line sensing works.
// tweak delay parameters in case of changes that aren't appropriate.
// change motion functions if nothing's happening altogether.

// initialise motors
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *leftRear = AFMS.getMotor(1);
Adafruit_DCMotor *rightRear = AFMS.getMotor(2);

// important variables in main
int frontArrayLineCounter = 0;
int backArrayLineCounter = 0;
int fls = 0;
int bls = 0;
int speed = 0;
float us = 0;
int starttime = 0;
int secondstage = 0;
int endtime = 0;
int type;
int under = 0;

void setup() {
  // set pin modes
  setPinModes();

  Serial.begin(9600);
  if (!AFMS.begin()) {         // create with the default frequency 1.6KHz
  // if (!AFMS.begin(1000)) {  // OR with a different frequency, say 1KHz
    Serial.println("Could not find Motor Shield. Check wiring.");
    while (1);
  }
  Serial.println("Motor Shield found.");
  // await button press to move onto loop
  attachInterrupt(digitalPinToInterrupt(buttonPin), toggleStart, CHANGE);
  while (!start){
    delay(10);
  }
  Serial.println("Beginning test run");
  digitalWrite(inOperationLed, HIGH);
}

void loop() {
  // follows line forwards and then backwards for a short period of time
  // observe and adjust controller gains
  // make sure torque vectoring always occurs on opposite side of coincident sensor
  for (int i = 0; i <= 10000; i++) {
    fls = frontLineSense();
    deafLineFollow(fls, 120, leftRear, rightRear, frontArrayLineCounter);
  }
  Serial.println(frontArrayLineCounter);
  stop(leftRear, rightRear);
  delay(1000);
  for (int i = 0; i <= 10000; i++) {
    bls = rearLineSense();
    highLineFollow(bls, 120, leftRear, rightRear, backArrayLineCounter);
  }
  Serial.println(backArrayLineCounter);
  stop(leftRear, rightRear);
  delay(1000);
}