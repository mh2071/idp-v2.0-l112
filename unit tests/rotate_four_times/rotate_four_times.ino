#include <Arduino.h>
#include <Adafruit_MotorShield.h>
#include "motion_funcs.h"
#include "led_signals.h"
#include "sensor_readers.h"
#include "Ticker.h"

// unit test for 4x90deg rotations both cw and ccw.

#define min_us_distance 8

// initialise motors

Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *leftRear = AFMS.getMotor(1);
Adafruit_DCMotor *rightRear = AFMS.getMotor(2);

// important variables
int frontArrayLineCounter = 0;
int backArrayLineCounter = 0;
int fls = 0;
int bls = 0;
int speed = 0;
float us = 0;
int starttime = 0;
int secondstage = 0;
int endtime = 0;
int type;
int under = 0;

void setup() {
// setting up pins
  setPinModes();
  Serial.begin(9600);
  if (!AFMS.begin()) {         // create with the default frequency 1.6KHz
  // if (!AFMS.begin(1000)) {  // OR with a different frequency, say 1KHz
    Serial.println("Could not find Motor Shield. Check wiring.");
    while (1);
  }
  Serial.println("Motor Shield found.");
  attachInterrupt(digitalPinToInterrupt(buttonPin), toggleStart, CHANGE);
  while (!start){
    delay(10);
  }
  Serial.println("Beginning test run");
  digitalWrite(inOperationLed, HIGH);
}

void loop() {
  // change rotation delay (3000) and speed to tune a full 360deg rotation.
  for (int i = 0; i < 4; i++) {
    rotate90(0, 3000, leftRear, rightRear, 160); // turn 90deg right four times
  }
  delay(1000);
  for (int i = 0; i < 4; i++) {
    rotate90(1, 3000, leftRear, rightRear, 160); // turn 90deg left four times
  }
  delay(1000);
}
