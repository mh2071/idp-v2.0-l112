#include <Arduino.h>
#include <Adafruit_MotorShield.h>
#include "motion_funcs.h"
#include "sensor_readers.h"
#include "Ticker.h"

// drops off one cube also identifying its foam porosity.

// constants for front sensors
#define min_us_distance 15
#define min_ir_distance 8

// initialising motors
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *leftRear = AFMS.getMotor(1);
Adafruit_DCMotor *rightRear = AFMS.getMotor(2);

// initialising variables
int frontArrayLineCounter = 0;
int backArrayLineCounter = 0;
int fls = 0;
int bls = 0;
int speed = 255;
float us = 0;
int starttime = 0;
int secondstage = 0;
int endtime = 0;
int type;
int under = 0;


void setup() {
  // setting pin modes, attaching pwm enabled pins to servo
  // returning servos to prescribed positions
  // awaiting button press to move onto main loop
  setPinModes();
  aperture.attach(aperturePin);
  height.attach(heightPin);
  servoOpen(height_min, height_max, height);
  delay(500);
  servoClose(aperture_min, aperture_max, aperture);
  Serial.begin(9600);
  if (!AFMS.begin()) {         // create with the default frequency 1.6KHz
  // if (!AFMS.begin(1000)) {  // OR with a different frequency, say 1KHz
    Serial.println("Could not find Motor Shield. Check wiring.");
    while (1);
  }
  Serial.println("Motor Shield found.");
  attachInterrupt(digitalPinToInterrupt(buttonPin), toggleStart, CHANGE);
  while (!start){
    delay(10);
  }
  Serial.println("Beginning test run");
  // amber led pin turned on, flashing handled by physical circuitry
  digitalWrite(inOperationLed, HIGH);
  starttime = millis();
}

void loop() {
  // single cube collection algorithm
  // generalisation found in searchAndDropStage within two_cube_collect 
  endtime = millis();
  // adequate timing to traverse ramp without any false infrared detects
  if ((endtime - starttime) < 15000) {
    fls = frontLineSense();
    deafLineFollow(fls, speed, leftRear, rightRear, frontArrayLineCounter);
    endtime = millis();
    Serial.println(endtime - starttime);
  } 
  // searching for cube with front sensor
  else if (infraredRead() > min_ir_distance){
    fls = frontLineSense();
    deafLineFollow(fls, 140, leftRear, rightRear, frontArrayLineCounter);
  }
  // cube collection
  else {
    stop(leftRear, rightRear);
    clawCollect(aperture, height);
    // cube type identified, see sensor_readers for further info and pertinent led signal
    type = cubeType();
    Serial.println("Collected.");
    // junctionDetect set false to overwrite prior triggers from starting box and dropoff junction
    junctionDetect = false;
    while (!junctionDetect) {
      bls = rearLineSense();
      highLineFollow(bls, speed, leftRear, rightRear, backArrayLineCounter);
    }
    // dropoff generalised to cube type
    rotate90(type, rotationGain, leftRear, rightRear, 160);
    clawDropoff(aperture, height);
    delay(100);
    rotate90(!type, rotationGain, leftRear, rightRear, 160);
    junctionDetect = false;
    // traversing two horizontal lines with rear sensor before stopping.
    while (!junctionDetect){
      highLineFollow(bls, speed, leftRear, rightRear, backArrayLineCounter);
    }
    junctionDetect = false;
    while (!junctionDetect){
      highLineFollow(bls, speed, leftRear, rightRear, backArrayLineCounter);
    } 
    stop(leftRear, rightRear);
    while(1);
}