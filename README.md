# IDP v2.0 L112

_**This is the code repo for IDP team 112.**_

Notes and points our code can be summarised as:

a) Always move all .h libraries from their dedicated folder to the folder of the respective test file you'd like to use
and back to a dedicated folder afterwards to prevent unwanted changes. This is due to an idiosyncracy of the Arduino IDE.

b) The natural sequence of unit tests is somewhat obvious, but we list them here regardless:
1.     First servo test
2.     Forward-backward linesense
3.     Rotate four times
4.     Ultrasound stopper
5.     Us stop and claw
6.     Falc test
7.     Junction detection
8.     Cube type test
9.     One cube collect
10.     First recognised dropoff
11.     Two cube collect
12.     Third cube sweep


c) Exercise caution, since due to mechanical delays and unfortunate events we were only able to test parameters and loop functions until x.
Nonetheless, we believe this may serve as brain food for those seeking not the fail the task as gloriously as we did.
