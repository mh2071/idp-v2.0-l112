#include <iostream>
#include <string>
using namespace std;


//test delay times for 90 deg rotation
//try these values i found, not sure if it's for the right motor tho
int rotateDelay = 1159;
void rotate90(bool direction) {
  //clockwise
  if (direction == 1){
    leftRear->run(FORWARD);
    rightRear->run(BACKWARD);
  } 
  //anticlockwise
  else if (direction == 0) {
    rightRear->run(FORWARD);
    leftRear->run(BACKWARD);
  }
  leftRear->setSpeed(200);
  rightRear->setSpeed(200);
  delay(rotateDelay);
  stop();
}

void stop() {
  leftRear->setSpeed(0);
  rightRear->setSpeed(0);
  leftRear->run(RELEASE);
  rightRear->run(RELEASE);
  delay(20);
}

void accelerate(int initialSpeed, int finalSpeed) {
  for (int i=initialSpeed; i<=finalSpeed; i++) {
    leftRear->setSpeed(i);
    rightRear->setSpeed(i);
    // currentSpeed = i;
  }
  leftRear->run(FORWARD);
  rightRear->run(FORWARD);
  delay(20);
}

void decelerate(int initialSpeed, int finalSpeed) {
  for (int i=initialSpeed; i>=finalSpeed; i--) {
    leftRear->setSpeed(i);
    rightRear->setSpeed(i);
    // currentSpeed = i;
  }
  leftRear->run(FORWARD);
  rightRear->run(FORWARD);
  delay(20);
}

void move(bool direction, int speed) {
  if (direction == 1) {
    leftRear->run(FORWARD);
    rightRear->run(FORWARD);
  } else if (direction == 0) {
    leftRear->run(BACKWARD);
    rightRear->run(BACKWARD);
  }
  leftRear->setSpeed(speed);
  rightRear->setSpeed(speed);
  delay(20);
}
