#include <Servo.h>

Servo aperture; // clamp aperture, on servo slot 2, 70 to 170
Servo direction;  // clamp direction, on servo slot 1, 10 to 100
// twelve servo objects can be created on most boards

int pos = 0;   // variable to store the servo position

void setup() {
  // attached to pin on board, don't connect anything else!
  aperture.attach(9); 
  direction.attach(10); 
}

void loop() {
  for (int i = 10; i<=100; i++){
    direction.write(i);
    delay(15);
  }
  delay(200);
  for (int i = 70; i<=170; i++){
    aperture.write(i);
    delay(15);
  }
  delay(200);
  for (int i = 10; i<=100; i++){
    direction.write(110-i);
    delay(15);
  }
  delay(200);
  for (int i = 70; i<=170; i++){
    aperture.write(240 - i);
    delay(15);
  }
  delay(1000);
}