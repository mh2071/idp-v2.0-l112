#include "sensor_readers.h"
#include "pin_allocations.h"

long ultrasoundRead() {
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  reading = (duration/2) / 29.1;
  distance = (alpha * reading) + (1.0 - alpha) * distance;
  return distance;
}

int lineSense(){
  if (digitalRead(lflsPin) == 0 and digitalRead(rflsPin) == 1) {
    return 2;
  } else if (digitalRead(rflsPin) == 1 and digitalRead(lflsPin) == 0){
    return 1;
  } else {
    return 0;
  }
}
