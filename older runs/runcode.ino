// include any important libraries here
#include <Adafruit_MotorShield.h>

// set important constants here: pin allocations, controller gains, sensor reference values.

// sensor references
#define lflsRef 0
#define lrlsRef 0
#define rrlsRef 0
#define rflsRef 0
#define ultrasoundRef 0

// pin allocations
#define lflsPin 0
#define lrlsPin 1
#define rrlsPin 2
#define rflsPin 3
#define echoPin 4
#define trigPin 5
#define coarseCubeLed 9
#define fineCubeLed 10
#define inOperationLed 11

// motor operator gains
#define lineFollowerGain 100
#define rotationGain 10

long duration, distance;

Adafruit_MotorShield AFMS = Adafruit_MotorShield();

// Motor slots. Ccw assignment starting from the left front
Adafruit_DCMotor *leftRear = AFMS.getMotor(2);
Adafruit_DCMotor *rightRear = AFMS.getMotor(3);
Adafruit_DCMotor *leftFront = AFMS.getMotor(1);
Adafruit_DCMotor *rightFront = AFMS.getMotor(4);

// conditionals for each stage of operation
bool start = true;

// sensor readings
int lfls = 0, rfls = 0, rrls = 0, lrls = 0;
int ultraDist = 0; // obstacle distance measurement from ultrasound in centimetres

bool setPinModes(lf,lr, rr, rf, tr, ec, ps) {
  pinMode(lf, INPUT);
  pinMode(lr, INPUT);
  pinMode(rr, INPUT);
  pinMode(rf, INPUT);
  pinMode(tr, OUTPUT);
  pinMode(ec, INPUT);
  pinMode(ps, INPUT);
  return true;
}

long ultrasoundRead(){
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = (duration/2) / 29.1;
  return distance;
}

void stopper(speed) {
  for (int i = speed; i <= 0; i++){
      leftRear->setSpeed(i);
      rightRear->setSpeed(i);
  }
}

void turner(direction) {
  leftRear->setSpeed(0);
  rightRear->setSpeed(0);
  delay(10);
  leftRear->setSpeed(1.5*rotationGain);
  rightRear->setSpeed(1.5*rotationGain);
  if (direction == "left"){
    leftRear->run(BACKWARD);
    rightRear->run(FORWARD);
  } else {
    leftRear->run(FORWARD);
    rightRear->run(BACKWARD);
  }
}

void goForward(speed) {
  leftRear->setSpeed(speed);
  rightRear->setSpeed(speed);
  leftRear->run(FORWARD);
  rightRear->run(FORWARD);
  delay(20);
  leftRear->run(RELEASE);
  rightRear->run(RELEASE);
}

void setup() {
  // setup code runs once. make it count.
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.println("Adafruit Motorshield v2 - DC Motor test!");

  if (!AFMS.begin()) {         // create with the default frequency 1.6KHz
  // if (!AFMS.begin(1000)) {  // OR with a different frequency, say 1KHz
    Serial.println("Could not find Motor Shield. Check wiring.");
    while (1);
    start = false;
  }
  Serial.println("Motor Shield found.");

  // sensor calibrations: make sure sensor readings are as expected at the start.
  // ----------- SENSOR CALIBRATION AND TESTS HERE -------------------

  // setting pin modes
  start = setPinModes();

  if (start) {
    digitalWrite(inOperationLed, HIGH);
    Serial.println("All tests complete, now commencing task!")
  }

  leftRear->setSpeed(1.5 * lineFollowerGain);
  rightRear ->setSpeed(1.5 * lineFollowerGain);

}

void loop() {
  if (start) {

    }
  }

}
