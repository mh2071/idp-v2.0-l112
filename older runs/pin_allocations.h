#define PIN_ALLOCATIONS_H

#include <Arduino.h>

// pin allocations
#define lflsPin 7
#define lrlsPin 4
#define rrlsPin 2
#define rflsPin 3
#define echoPin 8
#define trigPin 5
#define coarseCubeLed 9
#define fineCubeLed 10
#define inOperationLed 11
#define buttonPin 15

// important constants

#define alpha 0.1