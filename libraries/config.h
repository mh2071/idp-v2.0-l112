#include <Arduino.h>

// configuration library.

// pin allocations
#define IRPin A0
#define model 1080
#define lflsPin 2
#define lrlsPin 3
#define rrlsPin 4
#define rflsPin 5
#define echoPin 7
#define trigPin 6
#define coarseCubeLed 11
#define fineCubeLed 12
#define inOperationLed 13
#define aperturePin 10
#define buttonPin 8
#define heightPin 9
#define photosensorPin A2

// important constants
#define alpha 1
#define density_threshold 490
#define intended_speed 100

bool start = 0;

// set pin modes
void setPinModes() {
  pinMode(lflsPin, INPUT);
  pinMode(lrlsPin, INPUT);
  pinMode(rrlsPin, INPUT);
  pinMode(rflsPin, INPUT);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(photosensorPin, INPUT);
  pinMode(coarseCubeLed, OUTPUT);
  pinMode(fineCubeLed, OUTPUT);
  pinMode(inOperationLed, OUTPUT);
  pinMode(buttonPin, INPUT);
}

// toggles start. actuated by button press, stalls code within void setup() until button press
void toggleStart() {
  start = 1;
  Serial.println("Button pressed!");
}
