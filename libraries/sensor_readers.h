#include <Arduino.h>
#include <Adafruit_MotorShield.h>
#include "config.h"
#include <SharpIR.h>
#include "led_signals.h"

// initialise important variables
float duration;
float reading;
float distance = 200;
float initial_speed, final_speed;
float dense;
float density;
float sweep_read;
float sweep_dist;
int distance_ir;

// set up infrared sensor
SharpIR IRSensor = SharpIR(IRPin, model);

long ultrasoundRead() {
  // ultrasound reader with exponential moving average, not used
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  reading = (duration/2) / 29.1;
  distance = (alpha * reading) + (1.0 - alpha) * distance;
  return distance;
}

long ultrasoundSweep(float a) {
  // ultrasound sweeper, not used
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  sweep_read = (duration/2) / 29.1;
  sweep_dist = (a * sweep_read) + (1 - a) * sweep_dist;
  return sweep_dist;
}

int frontLineSense(){
  // uses front sensor array to indicate direction
  if (digitalRead(lflsPin) == 0 and digitalRead(rflsPin) == 1) {
    return 1; // leaning left
  } else if (digitalRead(rflsPin) == 0 and digitalRead(lflsPin) == 1){
    return 2; // leaning right
  } else if (digitalRead(rflsPin) == 1 and digitalRead(lflsPin) == 1){
    return 3; // on dash
  } else {
    return 0; //going straight
  }
}

int rearLineSense(){
  // uses rear sensor array to indicate direction
  if (digitalRead(lrlsPin) == 0 and digitalRead(rrlsPin) == 1) {
    return 1; // leaning right
  } else if (digitalRead(rrlsPin) == 0 and digitalRead(lrlsPin) == 1){
    return 2; // leaning left
  } else if (digitalRead(rrlsPin) == 1 and digitalRead(lrlsPin) == 1){
    return 3; // on dash
  } else {
    return 0; //going straight
  }
}

int cubeType(float a){
  // expect coarse to return 0 and fine 1.
  for (int i = 0; i < 100; i++){
    // averaging reading from ultrasound
    dense = analogRead(IRPin);
    density = (a * dense) + (1.0 - a) * density;
    delay(5);
  }
  if (density > density_threshold){
    collectionSignal(0);
    return 0;
  } else {
    collectionSignal(1);
    return 1;
  }
}

int adder(int a, int b){
  return a + b;
}

int infraredRead(){
  // infrared distance reader
  distance_ir = IRSensor.distance();
  return distance_ir;
}
