#include <Arduino.h>
#include "pin_allocations.h"

bool ledState = 0;

// running led signal for which a timer was first used.
// due to an extensive use of loops in an interrupt manner this solution would be too erratic
// a physical flashing solution was used instead
void runningBlink() {
    if (ledState){
      digitalWrite(inOperationLed,HIGH);
    }
    else {
      digitalWrite(inOperationLed,LOW);
    }
  Serial.println("Alternated");
  ledState = !ledState;
}

// collection led indicator
void collectionSignal(int type) {
  if (type = 0) {
    digitalWrite(coarseCubeLed, HIGH);
    digitalWrite(fineCubeLed, LOW);
  } else {
    digitalWrite(coarseCubeLed, LOW);
    digitalWrite(fineCubeLed, HIGH);
  }
}