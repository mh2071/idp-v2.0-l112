#include <Arduino.h>
#include <Adafruit_MotorShield.h>
#include <Servo.h>
#include <stdlib.h>

// motor operator gains
#define lineFollowerGain 1.6
#define rotationGain 4000
#define offsetCoefficient 1.05
#define aperture_min 0
#define aperture_max 180
#define height_min 75
#define height_max 150

// servo objects initialised in here to facilitate development
Servo aperture;
Servo height;

// delays and markers for junction detecion accuracy
int leftDetectTime = 1000;
int rightDetectTime = 0;
int junctionDelay = 800;
bool junctionDetect = false;

// stopper
void stop(Adafruit_DCMotor *lr, Adafruit_DCMotor *rr) {
  lr->setSpeed(0);
  rr->setSpeed(0);
  lr->run(RELEASE);
  rr->run(RELEASE);
  delay(20);
}

//test delay times for 90 deg rotation
//try these values i found, not sure if it's for the right motor tho
// direction 1 is right, 0 is left
// adjust rotation delay
void rotate90(bool direction, int rotateDelay, Adafruit_DCMotor *lr, Adafruit_DCMotor *rr, int speed) {
  if (direction){ 
    lr->run(FORWARD);
    rr->run(BACKWARD);
  } else {
    rr->run(FORWARD);
    lr->run(BACKWARD);
  }
  lr->setSpeed(speed);
  rr->setSpeed(speed);
  delay(rotateDelay);
  stop(lr, rr);
}

// incremental rotation about an instantaneous centre at the midpoint of the rear axis
void blockSweep(bool direction, Adafruit_DCMotor *lr, Adafruit_DCMotor *rr, int speed) {
  if (direction){ 
    lr->run(FORWARD);
    rr->run(BACKWARD);
  } else {
    rr->run(FORWARD);
    lr->run(BACKWARD);
  }
  lr->setSpeed(speed);
  rr->setSpeed(speed);
  stop(lr, rr); 
}

// acceleration for smoother torque delivery
void accelerate(int initialSpeed, int finalSpeed, Adafruit_DCMotor *lr, Adafruit_DCMotor *rr)  {
  for (int i=initialSpeed; i<=finalSpeed; i++) {
    lr->setSpeed(i);
    rr->setSpeed(i);
    // currentSpeed = i;
  }
  lr->run(FORWARD);
  rr->run(FORWARD);
  delay(20);
}

// deceleration for smoother torque delivery
void decelerate(int initialSpeed, int finalSpeed, Adafruit_DCMotor *lr, Adafruit_DCMotor *rr) {
  for (int i=initialSpeed; i>=finalSpeed; i--) {
    lr->setSpeed(i);
    rr->setSpeed(i);
    if (!i){
        lr->run(RELEASE);
        rr->run(RELEASE);
    } else {
        lr->run(FORWARD);
        rr->run(FORWARD);
    }
    // currentSpeed = i;
  }
  delay(20);
}

// 1 is forward, 0 is reverse
void move(bool direction, int speed, Adafruit_DCMotor *lr, Adafruit_DCMotor *rr) {
  lr->setSpeed(speed);
  rr->setSpeed(speed);
  if (direction) {
    lr->run(FORWARD);
    rr->run(FORWARD);
  } else if (!direction) {
    lr->run(BACKWARD);
    rr->run(BACKWARD);
  }
}

// "deaf" line follow due to default absense of ultrasound readings ib command. incorporates incremental torque vectoring for line following.
// no delay() commands to ensure accurate operation
void deafLineFollow(int fls, int speed, Adafruit_DCMotor *lr, Adafruit_DCMotor *rr, int fa){
  // see sensor_readers.h and frontLineSense() in particular for further information.
  if (fls == 0){
    move(1, speed, lr, rr);
    junctionDetect = false;
    } 
  else if (fls == 1){
      //leaning left
    leftDetectTime = millis();
    rr->setSpeed(0);
    lr->setSpeed(255);
    lr->run(FORWARD);
    rr->run(RELEASE);
    Serial.println("Turned right");
    junctionDetect = false;
    }
  else if (fls == 2) { // leaning right  
    rightDetectTime = millis();  
    lr->setSpeed(0);
    rr->setSpeed(255);
    lr->run(RELEASE);
    rr->run(FORWARD);
    Serial.println("Turned left");
    junctionDetect = false;
  }
  else {
    // case 1 for junction: simultaneous detection (highly unlikely for two line sensor configs)
    Serial.println("Triggered simultaneously.");
    fa++;
    junctionDetect = true;
    delay(100);
  }
  // set junctionDetect to true if two sensors triggered within a given period of time
  if (abs(rightDetectTime - leftDetectTime) < junctionDelay) {
    Serial.print("Time between triggers: ");
    Serial.println(abs(rightDetectTime - leftDetectTime));
    fa++;
    junctionDetect = true;
  }
  // counter incorporated for earlier purposes. not fully functional
  Serial.print("Front array count: ");
  Serial.println(fa);
}


// "high" because there would be a specific reason for why a deaf robot should decide to walk backwards
// backward line following, same reasoning as deafLineFollow above
void highLineFollow(int rls, int speed, Adafruit_DCMotor *lr, Adafruit_DCMotor *rr, int ra){
  if (rls == 0){
    move(0, speed, lr, rr);
    } 
  else if (rls == 1){
      //leaning left
    leftDetectTime = millis();
    lr->setSpeed(255);
    rr->setSpeed(0);    
    lr->run(BACKWARD);
    rr->run(RELEASE);
    Serial.println("Turned right");
    }
  else if (rls == 2) { // leaning right
    rightDetectTime = millis();  
    lr->setSpeed(0);
    rr->setSpeed(255);
    lr->run(RELEASE);
    rr->run(BACKWARD);
    Serial.println("Turned left");
  }
  else {
    junctionDetect = true;    
    ra++;
    delay(100);
  }
  // assumes junction if two sensors triggered within junctionDelay
  if (abs(rightDetectTime - leftDetectTime) < junctionDelay) {
    ra++;
    junctionDetect = true;
  }
  Serial.println(ra);
}

void servoOpen(int pos_min, int pos_max, Servo myservo){
  for (int pos = pos_min; pos <= pos_max; pos += 1) { // goes from 0 degrees to 180 degrees
  // in steps of 1 degree
    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15 ms for the servo to reach the position
  }  
}

void servoClose(int pos_min, int pos_max, Servo myservo){
  for (int pos = pos_max; pos >= pos_min; pos -= 1) { // goes from 180 degrees to 0 degrees
    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15 ms for the servo to reach the position
  }
}

void clawCollect(Servo aperture, Servo height) {
// collection sequence for claw: lower, close, raise  
  servoClose(height_min, height_max, height);
  delay(500);
  servoOpen(aperture_min, aperture_max, aperture);
  delay(500);
  servoOpen(height_min, height_max, height);
  delay(500);
}

void clawDropoff(Servo aperture, Servo height) {
  // dropoff sequence for claw: lower, open, raise
  servoClose(height_min, height_max, height);
  delay(500);
  servoClose(aperture_min, aperture_max, aperture);
  delay(500);
  servoOpen(height_min, height_max, height);
  delay(500);
}